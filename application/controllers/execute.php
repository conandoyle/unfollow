<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Execute extends CI_Controller
{
    public $acc_id;
	public function __construct()
    {
        parent::__construct();
        $this->load->library('curl');
		date_default_timezone_set("America/Los_Angeles");
    }
	
	public function index($id)
    {
        $max_time_exec = time() + 100;//Tối đa 110 giây
		
		//$this->acc_id = $this->input->post('acc_id') ? $this->input->post('acc_id') : 26;
		$this->acc_id = $id;
		
        $param = array(
            'acc_id'    => $this->acc_id,
            'proxy'     => true,
            'action_id' => 0,
        );
		
        $res = $this->curl->instadaily('account/get', true, $param);
		//print_r($res);die;
		
        $res = json_decode($res, true);

        if (empty($res['data'])) {
            exit;
        }
		
		
        $acc     = current($res['data']);
        $cookies = json_decode($acc['cookies'], true);
        $proxy   = $acc['proxy'];
		$proxy   = '';
		$insta_id = $acc['insta_id'];
		
		
		//$insta_id = '7804456339';
		
        $setting = $this->db->where('acc_id',$this->acc_id)->get('ufl_settings')->row_array();
		
		//End get info
		
		//Start get source
        $source  = $this->get_source($insta_id,$cookies,$proxy);
		//print_r($source);die;
		
		if($source == false) {
			
			$update_action = array(
				'error' => 1
			);
			$this->curl->instadaily('action/update/'.$acc['aca_id'], true, $update_action);
			exit;	
		}
		
		sleep(1);
		
		
		//STart exec
		$update_action = array(
			'results' => $acc['results']
		);
		$update_setting = array(
			'max_day' => $setting['max_day'],
			'max_ope' => $setting['max_ope']
		);
		$update_source = array();
		
		$this_total = count($source['users']);
		
		foreach($source['users'] as $row) 
		{
			if(time() >= $max_time_exec) {//Nếu vượt quá thời gian thực thi script là 110 giây thì thoát
				break;	
			}
			$row = current($row);
			
			$this_total--;
			
			if($setting['unfollow_all'] == 0 ){
				$user = $this->db->select('insta_id')->where('acc_id', $this->acc_id)->get('followings')->result_array();
				
			}
			if($row['followed_by_viewer'] == 0 || $row['requested_by_viewer'] == 1) {
				continue;	
			}
			/*if($setting['has_profile_img'] == 1 && $this->check_avatar($row['profile_pic_url']) === false) {
				continue;
			}
			
			if ($setting['private_user'] == 1 && $this->check_private($row['id']) == true) {
				continue;
			}*/
			$exec = $this->exec_unfollow($row['id'], $cookies, $proxy);
			print_r($exec);die;
			
			if($exec !== false) 
			{
				//Cập nhập lại cookie mới
				foreach($exec['cookies'] as $key => $cookie) {
					$cookies[$key] = $cookie;
				}
							
				$update_action['results']++;				
				$update_setting['max_day']--;
				$update_setting['max_ope']--;
				
				
				if($update_setting['max_day'] <= 0) {//Đạt giới hạn follow trong ngày
					
					$unfollow_day = json_decode($setting['unfollow_day'],true);
					$unfollow_ope = json_decode($setting['unfollow_ope'],true);
					$update_setting['max_ope'] = rand($unfollow_ope[0],$unfollow_ope[1]);
					$update_setting['max_day'] = rand($unfollow_day[0],$unfollow_day[1]);
					
					$update_action['time_run'] = $this->get_time($setting['delay_ope'],$setting['execute_time'],2);
					
					$update_source['next_page'] = '';
					
					break;	
				}
				
				if($update_setting['max_ope'] <= 0) {//Đạt giới hạn follow trong 1 lượt
					
					$unfollow_ope = json_decode($setting['unfollow_ope'],true);
					$update_setting['max_ope'] = rand($unfollow_ope[0],$unfollow_ope[1]);
					
					$get_time = $this->get_time($setting['delay_ope'],$setting['execute_time'],1);
					
					$update_action['time_run'] = $get_time['nexttime'];
					
					if($get_time['new_day'] == true) {
						$unfollow_day = json_decode($setting['unfollow_day'],true);
						$update_setting['max_day'] = rand($unfollow_day[0],$unfollow_day[1]);	
						
						$update_source['next_page'] = '';
					}
					
					break;	
				}
			
			} 
			else {
				$update_action['error'] = 1;
				break;
			}
			
			$sleep = json_decode($setting['delay_unfollow'],true);
			$sleep = rand($sleep[0],$sleep[1]);
			sleep($sleep);
			
		}
		
		
		
		
		//Nếu trả lời hết 12 người thì mới update next_page cho bảng source_users
		
		if($this_total == 0) {
			$update_source['next_page'] = $source['next_page'];
			if($source['end']) {
				$update_source['status'] = 2;	
			}
		}
		
		if($update_source) {
			$this->db->where('id',$source['id'])->set($update_source)->update('source_users');	
		}
		//Update bảng setting
		$this->db->where('id',$setting['id'])->set($update_setting)->update('ufl_settings');
		
		//Update account	
		$update_account = array(
			'cookies' => json_encode($cookies),
			'token'   => $cookies['csrftoken']
		);
		
	   $this->curl->instadaily('account/update/'.$this->acc_id, true, $update_account);
	   //update account action
	   $this->curl->instadaily('action/update/'.$acc['aca_id'], true, $update_action);

    } 
	
	
	
	
	private function get_source($insta_id,$cookies,$proxy)
    {
		$param = array(
			'query_hash' => '58712303d941c6855d4e888c5f0cd22f',
			'variables' => '{"id":"' . $insta_id . '","first":50}'
		);
		
        $header = array(
			//'x-csrftoken: ' . $cookies['csrftoken'],
            'cookie: ' . $this->cookie_string($cookies),
        );
		
		$param = http_build_query($param);
		
        $res = $this->curl->instagram('graphql/query/?'.$param, false, false, $header);
		
		$response = json_decode($res['response'],true);
		
		if($res['code'] != 200 || json_last_error() !== JSON_ERROR_NONE || !isset($response['status']) || $response['status'] != 'ok') {
			$log = array(
				'acc_id' => $this->acc_id,
				'type' => 'source',
				'code'   => $res['code'],
				'header' => '',
				'response' => $res['response'],
				'created' => time()
			);
			$this->db->insert('ufl_logs',$log);
			return false;
		}
		
		$users     =  $response['data']['user']['edge_follow']['edges'];
		//$next_page =  $response['data']['user']['edge_followed_by']['page_info']['end_cursor'];
		//$has_page  =  $response['data']['user']['edge_followed_by']['page_info']['has_next_page'];

        return array(
			//'id'        => $source['id'],
			'users'     => $users,
			//'next_page' => $next_page,
			//'end'       => $has_page ? 0 : 1,
		);
		
    }
	
	private function exec_unfollow($id, $cookies, $proxy)
    {
        $url = 'web/friendships/'.$id.'/unfollow/';
		
        $header = array(
			'content-type: application/x-www-form-urlencoded',
            'x-csrftoken: ' . $cookies['csrftoken'],
            'x-instagram-ajax: 8541abe490e3',
            'cookie: ' . $this->cookie_string($cookies),
            'x-requested-with: XMLHttpRequest'
        );

		$res = $this->curl->instagram($url,true,array('a'=>'b'),$header,'',true);
		
		
		$response = json_decode($res['response'],true);
		return $res;
		//Nếu bị lỗi thì return lỗi và kết quả báo lỗi
		if($res['code'] != 200 || json_last_error() !== JSON_ERROR_NONE || !isset($response['status']) || $response['status'] != 'ok') {
			$log = array(
				'acc_id'   => $this->acc_id,
				'type'     => 'exec',
				'code'     => $res['code'],
				'header'   => $res['header'],
				'response' => $res['response'],
				'created'  => time()
			);
			
			$this->db->insert('ufl_logs',$log);
				
			return false;
		}
		//không lỗi thì trả về success true
		return array(
			'success' => true,
			'cookies' => $this->get_cookie($res['header'])
		);	
		
    }  
	
	private function check_avatar($url)
	{
		if(preg_match('/\/5BBBE67A\//',$url)) 
			return false;
		else
			return true;
	}

	
	private function check_private($id)
	{
		$user = $this->getInfo($id);
		
		if($user && $user['is_private'] == true) {
			return true;	
		}
		
		return false;
	}

    
	private function get_time($delay,$exec_time,$type)
	{
		$timetoday = strtotime('TODAY');
		
		$exec_time = json_decode($exec_time,true);

		$start = $timetoday + $exec_time[0] * 3600;
			
		$end   = $timetoday + $exec_time[1] * 3600; 
		
		if($end < $start) 
		{
			if(intval(date('H')) < $exec_time[0]) {
				$start -= 86400;	
			} 
			else {
				$end += 86400;	
			}
		}
		
		if($type == 1) {
			
			$delay  = json_decode($delay,true);
			$delay  = rand($delay[0],$delay[1]) * 60;
			
			$nexttime = $delay + time();			
			
			$new_day = false;
						
			if($nexttime >= $end) {
				$nexttime = $start + 86400;	
				$new_day = true;
			}
			
			return array('nexttime'=>$nexttime,'new_day'=>$new_day);
		}
		
		else {
			$nexttime = $start + 86400;	
			return $nexttime;
		}
		
	}

    // thông tin user instagram
    private function getInfo($insta_id)
    {
        $url = 'https://i.instagram.com/api/v1/users/' . $insta_id . '/info/';
        $res = $this->curl->call($url);
        $res = json_decode($res['response'], true);
		
		if(isset($res['status']) && $res['status'] == 'ok'){
			return $res['user'];	
		}
		
		return array();
    }
	
	private function get_cookie($header)
    {
        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $header, $matches);

        $cookies = array();

        foreach ($matches[1] as $item) {
            parse_str($item, $cookie);
            if (!trim(str_replace('"', '', current($cookie)))) {
                continue;
            }
            $cookies = array_merge($cookies, $cookie);
        }
	
        return $cookies;
    }

    private function cookie_string($cookies)
    {
        $cookie_string = '';
        foreach ($cookies as $key => $cookie) {

            $cookie_string .= $key . '=' . $cookie . ';';
        }
        return $cookie_string;
    }
}
?>
<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Api extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('curl');
    }

    public function get($acc_id)
    {
        
        $setting = $this->db->where('acc_id', $acc_id)->get('ufl_settings')->row_Array();
       // $return = array(
//            'setting' => array(),
//            'source' => array()
//
//        );
//
//        if (!empty($setting)) {
//            $followSource = $this->db->where('setting_id', $setting['id'])->get('source_users')->result_array();
//            $return = array(
//                'setting' => $setting,
//                'source' => $followSource,
//            );
//
//        }

        echo response(200, $setting);

    }

    public function save($acc_id)
    {
        $setting = $this->input->post('setting');
		//$source = $this->input->post('source');
		
		if(!$setting){
			 echo response(400, 'Fields invalid');
            exit;
		}
		
		$setting = json_decode($setting,true);
		//$source  = json_decode($source,true);
		
		$check = $this->db->where('acc_id', $acc_id)->get('ufl_settings')->row_array();
		
		
		if($check) {//Update
			
			$this->db->where('id',$check['id'])->set($setting)->update('ufl_settings');
			
			//$source_old = $this->db->where('setting_id',$check['id'])->get('source_users')->result_array();
//			
//			$source_old_id = array_column($source_old,'pk');
//			
//			foreach($source as $key => $row) {
//				if (($old_key = array_search($row['pk'], $source_old_id)) !== false) {
//					
//					if($source_old[$old_key]['status'] != 4) {
//						unset($source_old[$old_key]); 	
//					}
//					
//					unset($source[$key]);
//				}
//				else {
//					$source[$key]['setting_id'] = $check['id'];
//				}
//			}
//			
//			if($source) {
//				$this->db->insert_batch('source_users', $source);
//			}
//			
//			if($source_old) {
//				
//				foreach($source_old as $key => $row) {
//					$source_old[$key]['status']    = ($row['status'] == 4) ? 1 : 4;
//					$source_old[$key]['next_page'] = '';	
//				}
//				
//				$this->db->update_batch('source_users',$source_old,'id');	
//			}
			
			echo response(200, 'update');
		}
		else {//insert
			$this->db->insert('ufl_settings', $data);
			//$insert_id = $this->db->insert_id();
//			foreach ($source as $key => $row) {
//               $source[$key]['setting_id'] = $insert_id;
//            }
//			$this->db->insert_batch('source_users', $source);
			echo response(200, 'insert');
		}
    }
} ?>